<!DOCTYPE html><!--Created by: Luis Cabrera-->
<html style="height: 100%;">
    <head>
        <title>Test</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body style="width: 100%;display: table;font-weight: 100;height: 100%;">
        <div class="container" style="vertical-align: middle; display: table-cell; text-align: center;">
            <div class="content" style="display: inline-block; width: 50%">
            <div style="text-align: left">                
                <?php 
                    $url= 'https://opentdb.com/api.php?amount=1&type=multiple';
                    $json = file_get_contents($url);
                    $data = json_decode($json, true);
                    $trivia = $data['results'][0];
                    /*
                    echo "<pre>";
                        print_r($trivia);
                    echo "</pre>";
                    */
                    $category = $trivia['category'];
                    $type = $trivia['type'];
                    $difficulty = $trivia['difficulty'];
                    $question = $trivia['question'];    //pregunta 
                    $answersX = $trivia['correct_answer'];      //respuesta correcta
                    $answers = $trivia['incorrect_answers'];    //respuestas incorrestas  
                    $answers[3] = $trivia['correct_answer'];    
                    $shuffleKeys = array_keys($answers); //ordenamiento aleatorio
                    shuffle($shuffleKeys);
                    $newArray = array();
                    foreach($shuffleKeys as $key) {
                        $i = $shuffleKeys[$key];
                        $newArray[$key] = $answers[$i];
                    }  
                 ?>
                <h4><?php echo $question; ?></h4>
                <button class="btn btn-primary btn-sm btn-block" onclick="answ(this.value)" value="<?php echo $newArray[0]; ?>">
                    <?php echo $newArray[0];?></button>
                <button class="btn btn-primary btn-sm btn-block" onclick="answ(this.value)" value="<?php echo $newArray[1]; ?>">
                    <?php echo $newArray[1];?></button>
                <button class="btn btn-primary btn-sm btn-block" onclick="answ(this.value)" value="<?php echo $newArray[2]; ?>">
                    <?php echo $newArray[2];?></button>
                <button class="btn btn-primary btn-sm btn-block" onclick="answ(this.value)" value="<?php echo $newArray[3]; ?>">
                    <?php echo $newArray[3];?></button>
            </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                var date = new Date();
                var count=1;    //contador de preguntas
                var point = getCookie("point"); //puntuacion
                count = getCookie("count");
                //alert("Date: "+date);
                //alert("Question: "+count);
                //alert("Score: "+point);
                if (count < 10) {
                    count++;
                    document.cookie = "count="+ count;
                    setTimeout(function(){ location = ''},10000); //cambia de pregunta pasado el tiempo
                }
                if (count >= 10) {
                    alert("Finish!");
                    alert("Score: "+point);
                    var param = {date : date, count : count, point : point};
                    $.ajax({    //envio el json a una pagina externa ejemplo
                        type: "post", 
                        url: 'http://opentdb.com/api.php', 
                        data: param,
                        success: function(data){
                            var obj = JSON.parse(data);
                            //alert("Succes");//codigo de exito
                        },
                        error: function(error){
                            //alert("Error");//codigo error
                        }
                    });
                    //window.location ="http://www.google.com/"; //redireccionamiento ejemplo
                }
            });
            function answ(value) {
                var count = getCookie("count");
                var point = getCookie("point"); //puntuacion
                var answer = "<?php echo $answersX; ?>";
                console.log(value);
                if (count < 10) {
                    if (value == answer) {
                        point++;
                        document.cookie = "point="+ point;
                        alert("Correct!"); 
                        window.location.reload();
                    }else{
                        alert("Wrong!"); 
                        window.location.reload();
                    }
                }
            }
            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i = 0; i <ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length,c.length);
                    }
                }
                return "";
            }
        </script>
    </body>
</html><!--Created by: Luis Cabrera-->
